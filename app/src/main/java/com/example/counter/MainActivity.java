package com.example.counter;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    int counter = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Variable declaration
        RelativeLayout rl = (RelativeLayout)findViewById(R.id.rltLayout);
        final TextView txtCounter = (TextView)findViewById(R.id.txtCounter);

        //When the user long presses anywhere in the screen, the counter should reset
        rl.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {

                counter = 0;
                txtCounter.setText(String.valueOf(counter));

                return true;
            }
        });

        //When user clicks anywhere in the screen, the counter gets added to one count
        rl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int store = counter++;

                txtCounter.setText(String.valueOf(store));
            }
        });
    }

    protected void onSaveInstanceState(Bundle outState){

        super.onSaveInstanceState(outState);

        final TextView txtCounter = (TextView)findViewById(R.id.txtCounter);

        int storeCount = Integer.parseInt(txtCounter.getText().toString());

        outState.putInt("count", storeCount);
    }

    protected void onRestoreInstanceState(Bundle savedState){

        super.onRestoreInstanceState(savedState);

        int getCount = savedState.getInt("count");

        final TextView txtCounter = (TextView)findViewById(R.id.txtCounter);

        txtCounter.setText(String.valueOf(getCount));

        counter = getCount;
    }
}